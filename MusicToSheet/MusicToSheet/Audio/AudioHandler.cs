﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NAudio.Wave;
using NAudio.CoreAudioApi;
using System.Diagnostics;

namespace MusicToSheet.Audio
{
	public class AudioHandler
	{
		private int _rate = 44100;
		private int _buffersize = (int)Math.Pow(2, 13);
		private WaveIn wi = new WaveIn();
		private static MMDeviceEnumerator _enumerator = new MMDeviceEnumerator();
		private MMDeviceCollection _devices = _enumerator.EnumerateAudioEndPoints(DataFlow.All, DeviceState.Active);
		private bool _recording = false;
		private MMDevice _device;

		public BufferedWaveProvider bwp;

		public AudioHandler()
		{
			wi.WaveFormat = new WaveFormat(_rate, 1);
			wi.BufferMilliseconds = (int)((double)_buffersize / (double)_rate * 300.0);
			wi.DataAvailable += new EventHandler<WaveInEventArgs>(AudioDataAvailable);
			bwp = new BufferedWaveProvider(wi.WaveFormat);
			bwp.BufferLength = _buffersize * 2;
			bwp.DiscardOnBufferOverflow = true;
		}

		public void ToggleListeningToMicrophone(int audioDeviceNumber = -1)
		{
			wi.DeviceNumber = audioDeviceNumber;
			try
			{
				if (!_recording)
				{
					wi.StartRecording();
					_recording = true;
				}
				else if (_recording)
				{
					wi.StopRecording();
					_recording = false;
				}
			}
			catch
			{
				string msg = "Could not record from audio device!\n\n";
				msg += "Is your microphone plugged in?\n";
				msg += "Is it set as your default recording device?";
				//MessageBox.Show(msg, "ERROR");
			}
		}

		public void SetDevice(MMDevice device)
		{
			_device = device;
		}

		public MMDeviceCollection GetDevices()
		{
			return _devices;
		}
		private void AudioDataAvailable(object sender, WaveInEventArgs e)
		{
			bwp.AddSamples(e.Buffer, 0, e.BytesRecorded);
		}

		public int GetMasterValue()
		{
			if (_device != null)
			{
				return (int)Math.Round(_device.AudioMeterInformation.MasterPeakValue * 100);
			}
			return 0;
		}
		public double AudioToFreq()
		{
			// check the incoming microphone audio
			int frameSize = _buffersize;
			var audioBytes = new byte[frameSize];
			bwp.Read(audioBytes, 0, frameSize);

			// return if there's nothing new to plot
			if (audioBytes.Length == 0)
				return 0;
			/*if (audioBytes[frameSize - 2] == 0)
				return "Nothing recorded";*/

			// incoming data is 16-bit (2 bytes per audio point)
			int BYTES_PER_POINT = 2;

			// create a (32-bit) int array ready to fill with the 16-bit data
			int graphPointCount = audioBytes.Length / BYTES_PER_POINT;

			// create double arrays to hold the data we will graph
			double[] pcm = new double[graphPointCount];
			double[] fft = new double[graphPointCount];
			double[] fftReal = new double[graphPointCount / 2];

			// populate Xs and Ys with double data
			for (int i = 0; i < graphPointCount; i++)
			{
				// read the int16 from the two bytes
				Int16 val = BitConverter.ToInt16(audioBytes, i * 2);

				// store the value in Ys as a percent (+/- 100% = 200%)
				pcm[i] = (double)(val) / Math.Pow(2, 16) * 200.0;
			}

			// calculate the full FFT
			fft = FFT(pcm);

			double max_magnitude = double.NegativeInfinity;
			int max_index = -1;
			for (int i = 0; i < _buffersize / 2 - 1; i++)
			{
				if (fft[i] > max_magnitude)
				{
					max_magnitude = fft[i];
					max_index = i;
				}
			}
			double freq = max_index * _rate / _buffersize;
			// just keep the real half (the other half imaginary)
			//Array.Copy(fft, fftReal, fftReal.Length);

			return freq;
		}

		public bool isRecording()
		{
			return _recording;
		}

		public double[] FFT(double[] data)
		{
			double[] fft = new double[data.Length];
			System.Numerics.Complex[] fftComplex = new System.Numerics.Complex[data.Length];
			for (int i = 0; i < data.Length; i++)
				fftComplex[i] = new System.Numerics.Complex(data[i], 0.0);
			Accord.Math.FourierTransform.FFT(fftComplex, Accord.Math.FourierTransform.Direction.Forward);
			for (int i = 0; i < data.Length; i++)
				fft[i] = fftComplex[i].Magnitude;
			return fft;
		}
	}
}
