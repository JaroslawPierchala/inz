﻿namespace MusicToSheet.models
{
	public class TimeSignature
	{
		public int Beats { get; set; }
		public int Mesure { get; set; }
	}
}
