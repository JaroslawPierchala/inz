﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicToSheet.models
{
	public class Sheet
	{
		public int Tempo { get; set; }
		public TimeSignature TimeSignature { get; set; }
		public Key Key { get; set; }
		public int Pitched { get; set; }
		public List<Note> Notes { get; set; }
		public string Title { get; set; }
		public string Composer { get; set; }
		public string Instrument { get; set; }
	}
}
