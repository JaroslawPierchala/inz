﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicToSheet.models
{
	public class Note
	{
		public Pitch Pitch { get; set; }
		public double Lenght { get; set; }

		public Note(double freq, double lenght)
		{
			Pitch = new Pitch(freq);
			Lenght = lenght;
		}

		public void SetLength(double LengthToTickRatio)
		{
			if (LengthToTickRatio <= 0.375)
			{
				Lenght = 0.25;
			}
			else if (LengthToTickRatio > 0.375 && LengthToTickRatio <= 0.625)
			{
				Lenght = 0.5;
			}
			else if (LengthToTickRatio > 0.625 && LengthToTickRatio <= 0.875)
			{
				Lenght = 0.75;
			}
			else if (LengthToTickRatio > 0.875 && LengthToTickRatio <= 1.25)
			{
				Lenght = 1;
			}
			else if (LengthToTickRatio > 1.25 && LengthToTickRatio <= 1.75)
			{
				Lenght = 1.5;
			}
			else if (LengthToTickRatio > 1.75 && LengthToTickRatio <= 2.5)
			{
				Lenght = 2;
			}
			else if (LengthToTickRatio > 2.5 && LengthToTickRatio <= 3.5)
			{
				Lenght = 3;
			}
			else if (LengthToTickRatio > 3.5 && LengthToTickRatio <= 4.5)
			{
				Lenght = 4;
			}
			else if (LengthToTickRatio > 4.5 && LengthToTickRatio <= 5.5)
			{
				Lenght = 5;
			}
			else if (LengthToTickRatio > 5.5 && LengthToTickRatio <= 6.5)
			{
				Lenght = 6;
			}
			else if (LengthToTickRatio > 6.5 && LengthToTickRatio <= 7.5)
			{
				Lenght = 7;
			}
			else if (LengthToTickRatio > 7.5)
			{
				Lenght = 8;
			}
		}
	}
}