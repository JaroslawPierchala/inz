﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MusicToSheet.Audio;
using System.Windows.Threading;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using MusicToSheet.models;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace MusicToSheet
{
	/// <summary>
	/// Logika interakcji dla klasy MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public AudioHandler audio = new AudioHandler();
		private DispatcherTimer RefreshRateTimer = new DispatcherTimer();
		private DispatcherTimer TempoTimer = new DispatcherTimer();
		//private System.Media.SoundPlayer player = new System.Media.SoundPlayer();
		private Stopwatch noteLengthCounter = new Stopwatch();
		private double oneBeatInMs;
		private bool Maximized = false;
		public Sheet sheet = new Sheet
		{
			Key = new models.Key { KeyNote = KeyNote.C, Scale = Scale.Major },
			Pitched = 0,
			Tempo = 90,
			TimeSignature = new TimeSignature { Beats = 4, Mesure = 4 },
			Notes = new List<Note>(),
			Title = "New Project",
			Composer = "Composer",
			Instrument = "Instrument"
		};

		public MainWindow()
		{
			InitializeComponent();
			this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
			RefreshRateTimer.Tick += dispatcherTimer_TickAsync;
			int ml = 60000 / sheet.Tempo;
			oneBeatInMs = 60000 / sheet.Tempo;
			int s = 0;
			if (ml >= 1000)
			{
				s = 1;
			}
			TempoTimer.Tick += TempoTimer_TickAsync;
			RefreshRateTimer.Interval = new TimeSpan(0, 0, 0, 0, 75);
			TempoTimer.Interval = new TimeSpan(0, 0, 0, s, ml);
			for (int i = 0; i < WaveIn.DeviceCount; i++)
			{
				var caps = WaveIn.GetCapabilities(i);
				DevicesComboBox.Items.Add(caps.ProductName.Substring(0, caps.ProductName.LastIndexOf("(")));
			}
			DevicesComboBox.SelectedIndex = 0;
			Setup();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			audio.ToggleListeningToMicrophone(DevicesComboBox.SelectedIndex);
			if (audio.isRecording())
			{
				TempoBox.IsEnabled = false;
				DevicesComboBox.IsEnabled = false;
				RefreshRateTimer.Start();
				TempoTimer.Start();
				RecordButtonText.Text = "Stop";
				if (DevicesComboBox.SelectedItem != null)
				{
					foreach (var device in audio.GetDevices())
					{
						if (device.ToString().Substring(0, device.ToString().LastIndexOf("(")) == DevicesComboBox.SelectedItem.ToString())
						{
							audio.SetDevice(device);
						}
					}
				}
			}
			else if (!audio.isRecording())
			{
				TempoBox.IsEnabled = true;
				DevicesComboBox.IsEnabled = true;
				RefreshRateTimer.Stop();
				TempoTimer.Stop();
				RecordButtonText.Text = "Nagrywaj";
			}
		}

		private void TempoBox_GotFocus(object sender, RoutedEventArgs e)
		{
			if (TempoBox.Text == "90")
			{
				TempoBox.Text = "";
			}
			TempoBox.Foreground = Brushes.Black;
		}

		private void TempoBox_LostFocus(object sender, RoutedEventArgs e)
		{
			TempoBox.Foreground = (Brush)new BrushConverter().ConvertFromString("#FF888888");
			if (TempoBox.Text == "")
			{
				TempoBox.Text = "90";
			}
			sheet.Tempo = int.Parse(TempoBox.Text);
			oneBeatInMs = 60000 / sheet.Tempo;
			int ml = 60000 / sheet.Tempo;
			int s = 0;
			if (ml >= 1000)
			{
				s = 1;
			}
			TempoTimer.Interval = new TimeSpan(0, 0, 0, s, ml);
		}

		private void dispatcherTimer_TickAsync(object sender, EventArgs e)
		{
			if (audio.GetMasterValue() > 3)
			{
				noteLengthCounter.Start();
				var freq = Task.Run(() => audio.AudioToFreq()).Result;
				if (freq > 87)
				{
					//ResultsNotes.Text += "Ferq - " + freq + "   ";
					var note = new Note(freq, 0);
					if (sheet.Notes.Count > 0)
					{
						if (!(sheet.Notes.Last().Pitch.NoteNumber == note.Pitch.NoteNumber
							&& sheet.Notes.Last().Pitch.Octave == note.Pitch.Octave))
						{
							noteLengthCounter.Stop();
							sheet.Notes.Last().SetLength(noteLengthCounter.ElapsedMilliseconds / oneBeatInMs);
							ResultsText.Text += $"{sheet.Notes.Last().Pitch.NoteName}{sheet.Notes.Last().Pitch.Octave}  {sheet.Notes.Last().Lenght}    ";
							//ResultsNotes.Text += $"{sheet.Notes.Last().Pitch.NoteName}{sheet.Notes.Last().Pitch.Octave}  {sheet.Notes.Last().Lenght}    ";
							ResultsNotes.Text += TextToNotes
								(sheet.Notes.Last().Pitch.NoteName, 
								sheet.Notes.Last().Pitch.Octave, 
								sheet.Notes.Last().Lenght);
							sheet.Notes.Add(note);
							noteLengthCounter.Reset();
						}
					}
					else
					{
						sheet.Notes.Add(note);
					}
				}
			}
			else
			{
				//var cos = Task.Run(() => audio.AudioToFreq()).Result;
				noteLengthCounter.Start();
				if (sheet.Notes.Count > 0)
				{
					if (sheet.Notes.Last().Pitch.NoteNumber != 12)
					{
						noteLengthCounter.Stop();
						sheet.Notes.Last().SetLength(noteLengthCounter.Elapsed.TotalMilliseconds / oneBeatInMs);
						ResultsText.Text += $"{sheet.Notes.Last().Pitch.NoteName}{sheet.Notes.Last().Pitch.Octave}  {sheet.Notes.Last().Lenght}    ";
						//ResultsNotes.Text += $"{sheet.Notes.Last().Pitch.NoteName}{sheet.Notes.Last().Pitch.Octave}  {sheet.Notes.Last().Lenght}    ";
						ResultsNotes.Text += TextToNotes
								(sheet.Notes.Last().Pitch.NoteName,
								sheet.Notes.Last().Pitch.Octave,
								sheet.Notes.Last().Lenght);
						var pause = new Note(0, 0);
						pause.Pitch.Octave = 0;
						pause.Pitch.NoteNumber = 12;
						pause.Pitch.NoteName = "Pause";
						sheet.Notes.Add(pause);
						noteLengthCounter.Reset();
					}
				}
			}
		}

		private void TempoTimer_TickAsync(object sender, EventArgs e)
		{
			//player.Play();
		}

		private void CloseButton_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.Shutdown();
		}

		private void GridTitleBar_MouseDown(object sender, MouseButtonEventArgs e)
		{
			DragMove();
		}

		private void MaximizeButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.WindowState == WindowState.Normal)
			{
				this.WindowState = WindowState.Maximized;
				Maximized = true;
			}
			else if (this.WindowState == WindowState.Maximized)
			{
				this.WindowState = WindowState.Normal;
				Maximized = false;
			}
		}

		private void MinimizeButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.WindowState == WindowState.Normal || this.WindowState == WindowState.Maximized)
			{
				this.WindowState = WindowState.Minimized;
			}
			else if (this.WindowState == WindowState.Minimized)
			{
				if (Maximized)
				{
					this.WindowState = WindowState.Maximized;
				}
				else
				{
					this.WindowState = WindowState.Normal;
				}
			}
		}

		private void PreviewTextInputNumbers(object sender, TextCompositionEventArgs e)
		{
			Regex regex = new Regex("[^0-9]+");
			e.Handled = regex.IsMatch(e.Text);
		}

		private void Setup()
		{
			TextBlock titleBlock = new TextBlock {
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Center,
				Text = sheet.Title,
				FontSize = 35,
				Margin = new Thickness(0, 10, 0, 0),
				FontFamily = new FontFamily("Times New Roman"),
				FontWeight = FontWeights.Bold,
				Name = "TitleTextBlock"
			};
			SheetGrid.Children.Add(titleBlock);
			titleBlock = new TextBlock
			{
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Left,
				Text = sheet.Instrument,
				FontSize = 20,
				Margin = new Thickness(5, 30, 0, 0),
				FontFamily = new FontFamily("Times New Roman"),
				Name = "ComposerTextBlock"
			};
			SheetGrid.Children.Add(titleBlock);
			titleBlock = new TextBlock
			{
				VerticalAlignment = VerticalAlignment.Top,
				HorizontalAlignment = HorizontalAlignment.Right,
				Text = sheet.Composer,
				FontSize = 18,
				Margin = new Thickness(0, 30, 5, 0),
				FontFamily = new FontFamily("Times New Roman"),
				Name = "InstrumentTextBlock"
			};
			SheetGrid.Children.Add(titleBlock);
			Canvas sheetCanvas = new Canvas {
				Margin = new Thickness(10, 50, 10, 20),
				HorizontalAlignment = HorizontalAlignment.Center,
				VerticalAlignment = VerticalAlignment.Top,
				Name = "SheetCanvas"
			};
			for (int i = 0; i < 5; i++)
			{
				Line line = new Line
				{
					Stroke = Brushes.Black,
					StrokeThickness = 2,
					X1 = 0,
					Y1 = i*2+10,
					X2 = sheetCanvas.ActualWidth,
					Y2 = i*2+10
				};
				sheetCanvas.Children.Add(line);
			}
			SheetGrid.Children.Add(sheetCanvas);
			List<models.Key> keys = new List<models.Key>
			{
				new models.Key { KeyNote = KeyNote.CSharp, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.ASharp, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.FSharp, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.DSharp, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.B, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.GSharp, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.E, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.CSharp, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.A, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.FSharp, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.D, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.B, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.G, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.E, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.C, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.A, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.F, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.D, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.BFlat, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.G, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.EFlat, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.C, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.AFlat, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.F, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.DFlat, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.BFlat, Scale = Scale.Minor },
				new models.Key { KeyNote = KeyNote.GFlat, Scale = Scale.Major },
				new models.Key { KeyNote = KeyNote.EFlat, Scale = Scale.Minor }
			};
			foreach (var key in keys)
			{
				KeyBox.Items.Add($"{key.KeyNote} {key.Scale}");
			}
			DisplayComboBox.Items.Add($"Notes");
			DisplayComboBox.Items.Add($"Text");
			DisplayComboBox.SelectedIndex = 0;
		}

		private void UpdateCanvas()
		{

		}

		private void BeatsTextBox_GotFocus(object sender, RoutedEventArgs e)
		{
			BeatsTextBox.Foreground = Brushes.Black;
		}

		private void BeatsTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			BeatsTextBox.Foreground = (Brush)new BrushConverter().ConvertFromString("#FF888888");
			if (BeatsTextBox.Text == "")
			{
				BeatsTextBox.Text = "4";
			}
			sheet.TimeSignature.Beats = int.Parse(BeatsTextBox.Text);
		}

		private void MesureTextBox_GotFocus(object sender, RoutedEventArgs e)
		{
			MesureTextBox.Foreground = Brushes.Black;
		}

		private void MesureTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			MesureTextBox.Foreground = (Brush)new BrushConverter().ConvertFromString("#FF888888");
			if (MesureTextBox.Text == "")
			{
				MesureTextBox.Text = "4";
			}
			sheet.TimeSignature.Mesure = int.Parse(MesureTextBox.Text);
		}

		private void DisplayComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(DisplayComboBox.SelectedIndex == 0)
			{
				ResultsNotes.Visibility = Visibility.Visible;
				ResultsText.Visibility = Visibility.Hidden;
			}
			else if (DisplayComboBox.SelectedIndex == 1)
			{
				ResultsNotes.Visibility = Visibility.Hidden;
				ResultsText.Visibility = Visibility.Visible;
			}
		}
		private char TextToNotes(string note, int octave, double length)
		{
			switch (note)
			{
				case "C":
					switch (octave)
					{
						default:
							break;
					};
					switch (length)
					{
						default:
							break;
					}
					break;
				case "C#/Db":;
					break;
				case "D":;
					break;
				case "D#/Eb":;
					break;
				case "E":;
					break;
				case "F":;
					break;
				case "F#/Gb":;
					break;
				case "G":;
					break;
				case "G#/Ab":;
					break;
				case "A":;
					break;
				case "A#/Bb":;
					break;
				case "B":;
					break;
			}
			return '0';
		}
	}
}
